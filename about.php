<?php
session_start();

if (!isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}

require 'function.php';

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>VOKASI CURHATIN</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/Logo.png" rel="icon">
  <link href="assets/img/Logo-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS Files -->
  <link href="assets/css/variables.css" rel="stylesheet">
  <link href="assets/css/main.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.php" class="logo d-flex align-items-center">
        <h1>VokasiCurhatin</h1>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="message.php">Message</a></li>
          <li><a href="account.php">Account</a></li>
          <li><a href="faq.php">FAQ</a></li>
      </nav>

      <!-- .navbar -->

      <div class="position-relative">
        <a href="about.php" class="mx-2"><span class="bi-info-circle"></span></a>
        <a href="#" class="mx-2 js-search-open"><span class=""></span></a>
        <i class="bi bi-list mobile-nav-toggle"></i>

        <!-- ======= Search Form ======= -->
        <div class="search-form-wrap js-search-form-wrap">
          <form action="search-result.html" class="search-form">
            <span class="icon bi-search"></span>
            <input type="text" placeholder="Search" class="form-control">
            <button class="btn js-search-close"><span class="bi-x"></span></button>
          </form>
        </div><!-- End Search Form -->

      </div>

    </div>

  </header><!-- End Header -->

  <main id="main">
    <section>
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-lg-12 text-center mb-5">
            <h1 class="page-title">About us</h1>
          </div>
        </div>

        <div class="row mb-5">

          <div class="d-md-flex post-entry-2 half">
            <a href="#" class="me-4 thumbnail">
              <img src="assets/img/img-about.jpg" alt="" class="img-fluid">
            </a>
            <div class="ps-md-5 mt-4 mt-md-0">
              <div class="post-meta mt-4">Webiste History</div>
              <h2 class="mb-4 display-4">Website History</h2>

              <p>Vokasi Curhatin adalah sebuah website untuk menampung curahan hati bagi para mahasiswa vokasi. Website
                ini tentunya bisa membuat mahasiswa memiliki perasaan lebih baik karena sudah mencurahkan isi hatinya.
              </p>

            </div>
          </div>


        </div>

      </div>
    </section>

    <section>
      <div class="container" data-aos="fade-up">
        <div class="row">
          <div class="col-12 text-center mb-5">
            <div class="row justify-content-center">
              <div class="col-lg-6">
                <h2 class="display-4">Our Team</h2>
                <p>Kami merupakan orang-orang dibalik terbuatnya website ini.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 text-center mb-5">
            <img src="assets/img/fotokosong.jpeg" alt="" class="img-fluid rounded-circle w-50 mb-4">
            <h4>Gilang Arya Sadewo W</h4>
            <span class="d-block mb-3 text-uppercase">Back End &amp; Front End Developer</span>
            <p>Nama Gilang, memiliki tugas untuk membuat back end dan front end. Ia merupakan mahasiswa Sekolah Vokasi IPB semester 5 dengan jurusan Teknologi Rekayasa Komputer.</p>
          </div>
          <div class="col-lg-4 text-center mb-5">
            <img src="assets/img/fotokosong.jpeg" alt="" class="img-fluid rounded-circle w-50 mb-4">
            <h4>Anggi Juvanro</h4>
            <span class="d-block mb-3 text-uppercase">Project Manager</span>
            <p>Nama Anggi, memiliki tugas sebagai Project Manager. Ia merupakan mahasiswa Sekolah Vokasi IPB semester 5 dengan jurusan Teknologi Rekayasa Komputer.</p>
          </div>
          <div class="col-lg-4 text-center mb-5">
            <img src="assets/img/fotokosong.jpeg" alt="" class="img-fluid rounded-circle w-50 mb-4">
            <h4>M. Rizal Yustriandi</h4>
            <span class="d-block mb-3 text-uppercase">UI/UX Designer</span>
            <p>Namanya Rizal, memiliki tugas untuk membuat UI/UX. Ia merupakan mahasiswa Sekolah Vokasi IPB semester 5 dengan jurusan Teknologi Rekayasa Komputer.</p>
          </div>
        </div>
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-legal">
      <div class="container">

        <div class="row justify-content-between">
          <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
            <div class="copyright">
              © Copyright <strong><span>VokasiCurhatin</span></strong>. All Rights Reserved
            </div>

            <div class="credits">
              Designed by <a href="/">VokasiCurhatin</a>
            </div>

          </div>

        </div>

      </div>
    </div>

  </footer>

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>