<?php
session_start();
require 'function.php';

if (!isset($_SESSION["login"])) {
  header("Location: login.php");
  exit;
}



$isipesan = query("SELECT * FROM kirimpesan");
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>VOKASI CURHATIN</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/Logo.png" rel="icon">
  <link href="assets/img/Logo-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS Files -->
  <link href="assets/css/variables.css" rel="stylesheet">
  <link href="assets/css/main.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header d-flex align-items-center fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <a href="index.php" class="logo d-flex align-items-center">
        <h1>VokasiCurhatin</h1>
      </a>

      <nav id="navbar" class="navbar">
        <ul>
          <li class="nav-item"><a class="nav-link active" href="index.php">Home</a></li>
          <li><a href="message.php">Message</a></li>
          <li><a href="account.php">Account</a></li>
          <li><a href="faq.php">FAQ</a></li>
      </nav>

      <!-- .navbar -->

      <div class="position-relative">
        <a href="#" class="mx-2 js-search-open"><span class="bi-search"></span></a>
        <a href="about.php" class="mx-2"><span class="bi-info-circle"></span></a>

        <i class="bi bi-list mobile-nav-toggle"></i>

        <!-- ======= Search Form ======= -->
        <div class="search-form-wrap js-search-form-wrap">
          <form action="search-result.html" class="search-form">
            <span class="icon bi-search"></span>
            <input type="text" placeholder="Search" class="form-control">
            <button class="btn js-search-close"><span class="bi-x"></span></button>
          </form>
        </div><!-- End Search Form -->

      </div>

    </div>

  </header><!-- End Header -->


  <main id="main">
    <!-- ======= Post Section ======= -->
    <section id="posts" class="posts">
      <?php foreach ($isipesan as $row) : ?>
        <div class="container" data-aos="fade-up">
          <div class="row g-5">
            <div class="col-lg-12 custom-border mb-3">
              <div style="min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px solid #e3e3e3;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.05);box-shadow:inset 0 1px 1px rgba(0,0,0,.05)">

                <div class="post-meta"> </div>
                <h5>To : <?php echo $row["to"] ?></h5>
                <h5>From : <?php echo $row["from"] ?></h5>
                <h5>Message: </h5>
                <h6 class="mb-4 d-block"><?php echo $row["message"] ?></h6>
                <a href="detailpesan.php?id=<?php echo $row["id_message"] ?>">
                  <h4>Comment (<?php echo jumlahkomentar($row['id_message']) ?>)</h4>


                </a>

                <!-- <input type="text" class="mt-2 col-md-12" id="komentar" name="komentar" placeholder="Comment....">

                <button class="mt-1 btn btn-info" type="button" id="kirim" name="kirim">Send Comment</button> -->
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>



    </section><!-- End Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">

    <div class="footer-legal">
      <div class="container">

        <div class="row justify-content-between">
          <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
            <div class="copyright">
              © Copyright <strong><span>VokasiCurhatin</span></strong>. All Rights Reserved
            </div>

            <div class="credits">
              Designed by <a href="/">VokasiCurhatin</a>
            </div>

          </div>

        </div>

      </div>
    </div>

  </footer>

  <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>