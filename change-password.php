<?php
session_start();
require 'function.php';
if (isset($_POST["reset"])) {
    $password = $_POST["password"];

    $email = $_SESSION["email"];

    $hash = password_hash($password, PASSWORD_DEFAULT);

    $result = mysqli_query($conn, "SELECT * FROM akun WHERE email='$email'");

    if ($email) {
        $new_password = $hash;
        mysqli_query($conn, "UPDATE akun SET password='$new_password' WHERE email='$email'");
?>
        <script>
            window.location.replace("login.php");
            alert("<?php echo "Password berhasil diubah" ?>");
        </script>
    <?php
    } else {
    ?>
        <script>
            alert("<?php echo "Please try again" ?>");
        </script>
<?php
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VOKASI CURHATIN- Forgot Password</title>
    <link href="assets/img/Logo.png" rel="icon">
    <link href="assets/img/Logo-icon.png" rel="apple-touch-icon">

    <!-- fonts-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- styles -->
    <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-warning">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-5 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Reset Password</h1>
                                        <p class="mb-4">Silahkan masukkan password baru</p>
                                    </div>
                                    <form class="user" action="" method="POST">
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="New Password..." required>
                                        </div>
                                        <button class="btn btn-primary btn-user btn-block" type="submit" id="reset" name="reset" value="reset">
                                            Reset Password
                                        </button>
                                    </form>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</body>

</html>