<?php $conn = mysqli_connect("localhost", "root", "", "vokasicurhatin");

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

function registrasi($data)
{
    global $conn;
    $username = strtolower(stripslashes($data["username"]));
    $email = htmlspecialchars($data["email"]);
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);

    //cek username sudah ada atau belum
    $result = mysqli_query($conn, "SELECT username FROM akun WHERE username = '$username'");

    if (mysqli_fetch_assoc($result)) {
        echo "<script>
            alert('username sudah terdaftar!')
        </script>";

        return false;
    }

    $hasil = mysqli_query($conn, "SELECT email FROM akun WHERE email = '$email'");

    if (mysqli_fetch_assoc($hasil)) {
        echo "<script>
        alert('email sudah terdaftar!')
    </script>";

        return false;
    }

    //cek konfirmasi password
    if ($password !== $password2) {
        echo "<script>
            alert('konfirmasi password tidak sesuai!')
        </script>";
        return false;
    }

    //enkripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    //tambahkan userbaru ke database
    mysqli_query($conn, "INSERT INTO akun VALUES('', '$username','$email','$password')");

    return mysqli_affected_rows($conn);
}

function pesan($kirim)
{
    global $conn;
    $to = htmlspecialchars($kirim["to"]);
    $from = htmlspecialchars($kirim["from"]);
    $message = htmlspecialchars($kirim["message"]);

    //query insert data
    $query = "INSERT INTO kirimpesan VALUE ('', '$to','$from','$message') ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function jumlahkomentar($idpesan)
{
    global $conn;

    $query = "SELECT * FROM komentar WHERE pesan = '$idpesan'";
    $res = mysqli_query($conn, $query);
    $row = mysqli_num_rows($res);

    return $row;
}

function detailpesan($idpesan)
{
    global $conn;
    $query = "SELECT * FROM kirimpesan WHERE id_message = '$idpesan'";
    $res = mysqli_query($conn, $query);

    $row = mysqli_fetch_assoc($res);

    return $row;
}

function tampilkomentar($idpesan)
{
    global $conn;

    $query = "SELECT * FROM komentar WHERE pesan = '$idpesan'";
    $res = mysqli_query($conn, $query);
    $row = [];

    while ($rows = mysqli_fetch_assoc($res)) {
        $row[] = $rows;
    }

    return $row;
}

function kirimkomentar($data, $idpesan)
{
    global $conn;
    $isi = $data['isi'];

    $query = "INSERT INTO komentar VALUE ('','$idpesan','$isi')";

    if (mysqli_query($conn, $query)) {
        echo "<div class='alert alert-success'>Sukses</div>";
    }
}
